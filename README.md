# Hnefatafl Graphics

This Repositority just contains a few graphics I've drawn for implementations of the ancient board game Hnefatafl.
All of them are released into Public Domain (as far possible in all other cases they could be used under the terms of CC0).

As far this Repositority contains:

* A full graphic pack for the irish variant Bradubh
 
![Brandubh Example](https://gitlab.com/The_Mighty_Glider/hnefataflgraphics/-/raw/master/brandubh/example.png?inline=false)

* A full graphic pack of pieces inspired by archiological findings

![Generic Example](https://gitlab.com/The_Mighty_Glider/hnefataflgraphics/-/raw/master/generic/example.png?inline=false)

### More is coming soon(TM) ;-)
